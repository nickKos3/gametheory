﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace BruteForce
{
    class KindOfBackpackProblem
    {
        private static List<int> bestAllocation;
        private static List<double> bestRealAllocation;
        private static double bestPrice;
        private static List<int> inequalitySignsForBestAllocation;
        private static double epsilon;

        private static List<int> _UtilityVector { get; set; }
        private static List<double> _EnemyAllocation;
        private static int _AmountOfResources;
        private static int _AmountOfObjects;

        public static (double, List<double>) MainFunction(List<int> UtilityVector, List<double> EnemyAllocation, int AmountOfResources, int AmountOfObjects)
        {
            _UtilityVector = UtilityVector;
            _EnemyAllocation = EnemyAllocation;
            _AmountOfResources = AmountOfResources;
            _AmountOfObjects = AmountOfObjects;

            if ((bestAllocation != null) && bestAllocation.Count > 0)
                bestAllocation.Clear();
            if ((bestRealAllocation != null) && bestRealAllocation.Count > 0)
                bestRealAllocation.Clear();
            if ((inequalitySignsForBestAllocation != null) && inequalitySignsForBestAllocation.Count > 0)
                inequalitySignsForBestAllocation.Clear();
            bestPrice = 0;
            epsilon = 0;

            var (price, allocation) = MaximizationOfBenefit(UtilityVector: UtilityVector,
                EnemyAllocation: EnemyAllocation,
                AmountOfResources: AmountOfResources,
                AmountOfObjects: AmountOfObjects);
            Console.WriteLine($"bestPrice - {price}");
            var appropriateAllocation = new List<double>(allocation);
            return (price, appropriateAllocation);
        }

        public static (double, List<double>) MaximizationOfBenefit(List<int> UtilityVector, List<double> EnemyAllocation, int AmountOfResources, int AmountOfObjects)
        {
            var flagsForUtilityFunc = GetAllNSets(AmountOfObjects);
            flagsForUtilityFunc.Reverse();
            var allInequalitySignCombs = GetAllNSets(AmountOfObjects, 3);
            //PrintMatrix(allInequalitySignCombs);
            
            foreach (var inequalitySignComb in allInequalitySignCombs)
            {
                Console.Write("inequalitySignComb   ");
                ShowInequalitySigns(inequalitySignComb);
                Console.WriteLine();
                foreach (var oneFlagForUtilityFunc in flagsForUtilityFunc)
                {
                    CheckSet(oneFlagForUtilityFunc, inequalitySignComb);
                }
            }
                
            
            var ll = GetBestSet();
            Console.Write("bestAllocation:  ");
            foreach (var el in ll)
            {
                Console.Write($"{el} ");
            }
            Console.WriteLine();

            Console.Write("bestRealAllocation:  ");
            foreach (var el in bestRealAllocation)
            {
                Console.Write($"{el} ");
            }
            Console.WriteLine();

            ShowInequalitySignsForBestAllocation();
            return (bestPrice, bestRealAllocation);
        }

        private static double CalcBenefit(List<int> vector, List<int> inequalitySigns, double curWeight)
        {
            double sumW = 0;
            epsilon = _AmountOfResources - curWeight;
            
            for (var i = 0; i <vector.Count; i++)
            {
                var temp = 0;
                if (inequalitySigns[i] <= 1 && _EnemyAllocation[i] == 0)
                    continue;
                //if (inequalitySigns[i] == 2 && epsilon > 0)
                //{
                //    temp = vector[i] * _UtilityVector[i];
                //}
                if (inequalitySigns[i] >= 1)
                {
                    temp = vector[i] * _UtilityVector[i] / (3 - inequalitySigns[i]);
                    if (inequalitySigns[i] == 2 && epsilon == 0)
                        temp /= 2;
                }
                sumW += temp;
            }

            return sumW;
        }

        private static (double, List<double>) CalcWeigth(List<int> vector, List<int> inequalitySigns)
        {
            double sumW = 0,
                benefitCounter = 0;
            var epsList = new List<double>();
            for (var i = 0; i < vector.Count; i++)
            {
                if (inequalitySigns[i] >= 1)
                    sumW += vector[i] * _EnemyAllocation[i];
                if (inequalitySigns[i] > 1)
                    benefitCounter++;
                epsList.Add(0);
            }
            epsilon = _AmountOfResources - sumW;
            if (epsilon > 0)
            {
                var partEps = epsilon / benefitCounter;
                for (var i = 0; i < vector.Count; i++)
                {
                    if (inequalitySigns[i] > 1)
                        epsList[i] = partEps;
                }
            }
            return (sumW, epsList);
        }

        private static List<double> FindAppropriateAllocation(List<int> vector, List<int> inequalitySigns)
        {
            List<double> allocation = new List<double>();
            double sumW = 0;
            int benefitObjects = 0;
            for (var i = 0; i < vector.Count; i++)
            {
                double temp = 0;
                if (inequalitySigns[i] >= 1)
                {
                    temp = vector[i] * _EnemyAllocation[i];
                    sumW += temp;
                    if (inequalitySigns[i] > 1)
                        benefitObjects++;
                }
                allocation.Add(temp);
            }
            epsilon = _AmountOfResources - sumW;
            if (epsilon > 0)
            {
                var partEpsilon = epsilon / benefitObjects;
                var benefitCounter = 0;
                for (var i = 0; i < vector.Count && partEpsilon > 0; i++)
                {
                    if (inequalitySigns[i] == 2)
                    {
                        allocation[i] += partEpsilon;
                        benefitCounter++;
                    }
                }
                if (benefitCounter != benefitObjects)
                    allocation.Clear();
            }
            return allocation;
        }

        private static double CalcBenefitOfAllocation(List<double> allocation)
        {
            double sumW = 0;
            for (var i = 0; i < allocation.Count; i++)
            {
                if (allocation[i] == _EnemyAllocation[i] && _EnemyAllocation[i] != 0)
                    sumW += _UtilityVector[i] / 2;
                if (allocation[i] > _EnemyAllocation[i])
                    sumW += _UtilityVector[i];
            }

            return sumW;
        }

        private static void CheckSet(List<int> items, List<int> inequalitySigns)
        {
            if (bestAllocation == null)
            {
                var (cW, eps) = CalcWeigth(items, inequalitySigns);
                if (cW <= _AmountOfResources)
                {
                    var alloc = FindAppropriateAllocation(items, inequalitySigns);
                    bestPrice = CalcBenefitOfAllocation(alloc);
                    bestAllocation = items;
                    bestRealAllocation = alloc;
                    inequalitySignsForBestAllocation = inequalitySigns;

                    ShowInequalitySigns(items);
                    Console.WriteLine($"cW - {cW}, bestPrice - {bestPrice}");
                }
            }
            else
            {
                var (cW, epsList) = CalcWeigth(items, inequalitySigns);
                var cC = CheckConditions(items, inequalitySigns, epsList);
                if (cW <= _AmountOfResources && cC)
                {
                    var alloc = FindAppropriateAllocation(items, inequalitySigns);
                    if (alloc.Count == 0) return;
                    var cB = CalcBenefitOfAllocation(alloc);
                    if (cB > bestPrice)
                    {
                        bestAllocation = items;
                        bestPrice = cB;
                        inequalitySignsForBestAllocation = inequalitySigns;
                        bestRealAllocation = alloc;

                        ShowInequalitySigns(items);
                        Console.WriteLine($"cW - {cW}, bestPrice - {bestPrice}");
                    }
                }
            }
        }

        public static bool CheckConditions(List<int> allocation, List<int> inequalitySigns, List<double> epsList)
        {
            for (int i = 0; i < _AmountOfObjects; i++)
            {
                switch (inequalitySigns[i])
                {
                    case 0: // less
                        if (allocation[i] * _EnemyAllocation[i] + epsList[i] >= _EnemyAllocation[i])
                            return false;
                        break;
                    case 1: // equal
                        if (allocation[i] * _EnemyAllocation[i] + epsList[i] == _EnemyAllocation[i])
                            break;
                        else return false;
                    case 2: // more
                        if (allocation[i] * _EnemyAllocation[i] + epsList[i] <= _EnemyAllocation[i])
                            return false;
                        break;
                    //default:
                    //    if (allocation[i] * _EnemyAllocation[i] < _EnemyAllocation[i])    // + epsilon
                    //        return false;
                    //    break;
                }
            }
            var sum = 0;
            foreach (var i in allocation)
            {
                if (i > 1) return false;
                sum += i;
            }
            if (sum > _AmountOfResources)
                return false;
            return true;
        }

        public static List<int> GetBestSet()
        {
            return bestAllocation;
        }

        public static void ShowInequalitySignsForBestAllocation()
        {
            Console.WriteLine("inequalitySignsForBestAllocation:");
            ShowInequalitySigns(inequalitySignsForBestAllocation);
        }

        public static void ShowInequalitySigns(List<int> inequalitySigns)
        {
            foreach (var el in inequalitySigns)
            {
                Console.Write($"{el} ");
            }
            Console.WriteLine();
        }

        private static List<List<int>> GetAllNSets(int n, int root = 2)
        {
            var allNSets = new List<List<int>>();
            var allowedSet = new List<int>();
            for (int i = 0; i < root; i++)
            {
                allowedSet.Add(i);
            }
            for (int i = 0; i < n; i++)
            {
                if (i != 0)
                {
                    var tempListOfLists = new List<List<int>>();
                    foreach (var el in allNSets)
                    {
                        var newList = new List<int>(el);
                        for (int j = 0; j < root; j++)
                        {
                            var innerList = new List<int>(newList);
                            innerList.Add(allowedSet[j]);
                            tempListOfLists.Add(innerList);
                        }
                    }
                    allNSets.Clear();
                    allNSets.AddRange(tempListOfLists);
                } else
                {
                    foreach (var el in allowedSet)
                    {
                        var newList = new List<int>() { el };
                        allNSets.Add(newList);
                    }
                }
            }
            return allNSets;
        }

        public static void PrintMatrix(List<List<int>> arr)
        {
            foreach (var ll in arr)
            {
                foreach (var el in ll)
                {
                    Console.Write($"{el} ");
                }
                Console.WriteLine();
            }
        }
    }
}
