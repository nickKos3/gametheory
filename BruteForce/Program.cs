﻿using System;
using System.Collections.Generic;

namespace BruteForce
{
    class Program
    {
        static void Main(string[] args)
        {
            int AmountOfResourcesOfFirPlayer = 3,
                AmountOfResourcesOfSecPlayer = 3,
                AmountOfObjects = 3;
            var UtilityVectorOfFirPlayer = new List<int>() { 7, 4, 2 };
            var UtilityVectorOfSecPlayer = new List<int>() { 2, 5, 5 };
            var EnemyAllocationSuggestion = new List<double>() { 0, 0, 3 };

            //GameBlotto gameBlotto = new GameBlotto(n, UtilityVectorOfFirPlayer, UtilityVectorOfSecPlayer);
            //gameBlotto.Play();
            //f1();
            //f2();
            List<double> tempBestAllocationOfFirPl;
            var tempBestAllocationOfSecPl = new List<double>(EnemyAllocationSuggestion);
            double tempBestPriceOfFirPl, tempBestPriceOfSecPl;
            for (int i = 0; i < 10; i++)
            {
                Console.Write($"allocation of sec player: ");
                foreach (var item in tempBestAllocationOfSecPl)
                {
                    Console.Write($"{item} ");
                }
                (tempBestPriceOfFirPl, tempBestAllocationOfFirPl) = KindOfBackpackProblem.MainFunction(
                    UtilityVector: UtilityVectorOfFirPlayer,
                    EnemyAllocation: tempBestAllocationOfSecPl,
                    AmountOfResources: AmountOfResourcesOfFirPlayer,
                    AmountOfObjects: AmountOfObjects);
                Console.WriteLine("---------------------------------------------");

                Console.Write($"allocation of fir player: ");
                foreach (var item in tempBestAllocationOfFirPl)
                {
                    Console.Write($"{item} ");
                }
                (tempBestPriceOfSecPl, tempBestAllocationOfSecPl) = KindOfBackpackProblem.MainFunction(
                    UtilityVector: UtilityVectorOfSecPlayer,
                    EnemyAllocation: tempBestAllocationOfFirPl,
                    AmountOfResources: AmountOfResourcesOfSecPlayer,
                    AmountOfObjects: AmountOfObjects);
                Console.WriteLine("---------------------------------------------");
            }
            
            Console.ReadKey();
        }

        public static void f1()
        {
            int NumOfResources = 5,
                NumOfObjects = 3;
            var firPermutations = MakeAllPermutations(NumOfResources, NumOfObjects);
            num = 1;
            var secPermutations = MakeAllPermutations(NumOfResources, NumOfObjects);
            var statisticList = new List<statisticOfPermutation>();
            foreach (var firPerm in firPermutations)
            {
                var oneTempStat = new statisticOfPermutation();
                foreach (var secPerm in secPermutations)
                {
                    int tempMarker = 0;
                    for (int i = 0; i < firPerm.Count; i++)
                    {
                        if (firPerm[i] > secPerm[i]) tempMarker++;
                        else if (firPerm[i] < secPerm[i]) tempMarker--;
                    }
                    if (tempMarker == 0) oneTempStat.draw++;
                    else if (tempMarker > 0) oneTempStat.win++;
                    else oneTempStat.lose++;

                }
                statisticList.Add(oneTempStat);
                Console.Write($"{statisticList.Count}  ");
                oneTempStat.WriteItInConsole();
            }
        }

        public static void f2()
        {
            int perm = 100;
            Console.WriteLine($"permutations of {perm} is {amountOfPermutations(perm, perm)}");
        }

        public static List<List<int>> MakeAllPermutations(int NumOfResources, int NumOfObjects)
        {
            var listOfDistributionPairs = GetDistributionPairs(NumOfResources, NumOfObjects);
            var listOfWholeDistributions = new List<List<int>>();
            Console.WriteLine(listOfDistributionPairs.Count);
            //for (int i = 0; i < listOfList.Count; i++)
            foreach (var list in listOfDistributionPairs)
            {
                if (list.Count > NumOfObjects) continue;
                var tempList = new List<int>(list);
                tempList.Sort();
                CheckAndAddZeros(ref tempList, NumOfObjects);
                //var temp =
                //listOfWholeDistributions.Add(tempList.);
                AddNewList(listOfWholeDistributions, tempList);
                Print(ref tempList);
                while (NextSet(ref tempList))
                {
                    //listOfWholeDistributions.Add(tempList);
                    AddNewList(listOfWholeDistributions, tempList);
                    Print(ref tempList);
                }
            }
            return listOfWholeDistributions;
        }

        public static void AddNewList(List<List<int>> list, List<int> el)
        {
            var newEl = new List<int>(el);
            list.Add(newEl);
        }

        public static List<List<int>> GetDistributionPairs (int number, int limitAmount)
        {
            var distributionPairs = new List<List<int>>();
            var firTempList = new List<int>();
            firTempList.Add(number);
            distributionPairs.Add(firTempList);
            while (distributionPairs[distributionPairs.Count - 1][0] > 1)
            {
                if (true)//tempList.Count < limitAmount)
                {
                    var tempList = new List<int>(distributionPairs[distributionPairs.Count - 1]);
                    int minPos = 0;
                    int remainder = 0;
                    for (int i = tempList.Count - 1; i >= 0; i--)
                    {
                        if (tempList[i] > 1 && tempList[i] <= tempList[minPos])
                        {
                            minPos = i;
                            break;
                        } else
                        {
                            remainder += tempList[i];
                            tempList.RemoveAt(i);
                        }
                    }
                    tempList[minPos]--;
                    if (remainder == 0)
                    {
                        tempList.Add(1);
                    } else
                    {
                        remainder++;    //cause of tempList[minPos]--;
                        while (remainder > 0)
                        {
                            if (remainder <= tempList[minPos])
                            {
                                tempList.Add(remainder);
                                remainder = 0;
                            }
                            else
                            {
                                tempList.Add(tempList[minPos]);
                                remainder -= tempList[minPos];
                                minPos++;
                            }
                        }
                    }
                    distributionPairs.Add(tempList);
                }
            }

            return distributionPairs;
        }

        static int num = 1; // номер перестановки
        public static void Print(ref List<int> a)
        {
            Console.Write(num++ + ": ");
            for (int i = 0; i < a.Count; i++)
                Console.Write(a[i] + " ");
            Console.WriteLine();
        }

        public static int amountOfPermutations(int n, int k)
        {
            if (k > 0 && k <= n)
                return amountOfPermutations(n, k - 1) + amountOfPermutations(n - k, k);
            else if (k > n)
            {
                return amountOfPermutations(n, n);
            }
            else if (k == 0 && n == 0)
            {
                return 1;
            }
            else return 0;
        }

        public static void swap(ref List<int> list, int i, int j)
        {
            int t = list[i];
            list[i] = list[j];
            list[j] = t;
        }

        public static List<int> CheckAndAddZeros(ref List<int> list, int n)
        {
            while (list.Count < n)
            {
                list.Insert(0, 0);
            }
            return list;
        }

        public static bool NextSet(ref List<int> list)
        {
            int j = list.Count - 2;
            while (j != -1 && list[j] >= list[j + 1]) j--;
            if (j == -1)
                return false; // больше перестановок нет
            int k = list.Count - 1;
            while (list[j] >= list[k]) k--;
            swap(ref list, j, k);
            int l = j + 1, r = list.Count - 1; // сортируем оставшуюся часть последовательности
            while (l < r)
                swap(ref list, l++, r--);
            return true;
        }
    }

    class GameBlotto
    {
        public GameBlotto(int amountOfObjects, List<int> l1, List<int> l2)
        {
            this.AmountOfObjects = amountOfObjects;
            this.Players.Add(new Player(amountOfResources: 3, utilityVector: l1));
            this.Players.Add(new Player(amountOfResources: 3, utilityVector: l2));
        }
        public int AmountOfObjects { set; get; }
        public List<Player> Players;

        public void Play()
        {
            Console.WriteLine("Start of game");
            var strategiesOfFirstPl = new List<List<int>>();
            var strategiesOfSecondPl = new List<List<int>>();
            
            var tempObjects = new List<int>(new int[this.AmountOfObjects]);
            int runner = 0;
            tempObjects[runner] = this.Players[0].AmountOfResources;
            //for (int i = 0; i < )
        }
    }

    class Player
    {
        public Player(int amountOfResources, List<int> utilityVector)
        {
            AmountOfResources = amountOfResources;
            UtilityVector = new List<int>(utilityVector);
        }

        public int AmountOfResources { set; get; }
        public List<int> UtilityVector { set; get; }

        public static void setUtilityVector(int AmountOfObjects = -1)
        {
            if (AmountOfObjects == -1)
            {
                Console.WriteLine("Enter AmountOfObjects = ");
                string str = Console.ReadLine();
                bool readOk = Int32.TryParse(str, out AmountOfObjects);
                if (readOk == false)
                {
                    Console.WriteLine("Failed reading of AmountOfObjects");
                }
            }

            for (int i = 0; i < AmountOfObjects; i++)
            {

            }
                
        }
        //public static Player GetPlayer (int amountOfResources, )
    }

    class statisticOfPermutation
    {
        public int win { set; get; }
        public int draw { set; get; }
        public int lose { set; get; }

        public void WriteItInConsole()
        {
            Console.WriteLine($"win - {this.win}; draw - {draw}; lose - {lose}");
        }
    }
}
